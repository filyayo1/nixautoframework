package nix.chetotam.examination.spec

import geb.spock.GebReportingSpec
import nix.chetotam.examination.page.BlogPage
import nix.chetotam.examination.page.StartPage

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Blog page"(){

        when:
            to StartPage
        and:
            "User goes to the Blog page"()
        then:
            at BlogPage


    }
}
