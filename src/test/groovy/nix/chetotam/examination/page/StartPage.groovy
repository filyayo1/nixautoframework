package nix.chetotam.examination.page

import geb.Page

class StartPage extends  Page{

    static content = {
        blogLink (wait: true) {$("a", text: "Blog")}
    }

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def "User goes to the Blog page"(){
        blogLink.click()
    }
}
