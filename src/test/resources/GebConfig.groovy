
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver

System.setProperty("geb.build.reportsDir", "target/geb-reports")
System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com")
//System.setProperty("autoqa.remoteUrl", "http://192.168.0.104:4444/wd/hub")
//System.setProperty("autoqua.remoteUrl", "http://10.12.73.230:4444/wd/hub")
System.setProperty("autoqua.remoteUrl", "http://localhost:4444/wd/hub")

hubUrl = new URL(System.getProperty("autoqua.remoteUrl"))

driver = {
    def driver = new ChromeDriver()
    driver.manage().window().maximize()
    return driver
}

environments{
    'filyayo-remote-chrome'{
        driver = {
            ChromeOptions options = new ChromeOptions()
            DesiredCapabilities capabilities = DesiredCapabilities.chrome()
            capabilities.setCapability(ChromeOptions.CAPABILITY, options)
            def driver = new RemoteWebDriver(hubUrl, capabilities)
            driver.manage().window().maximize()
            return driver
        }
    }
}